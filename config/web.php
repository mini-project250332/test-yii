<?php return [
    'id' => 'votre_application_id',
    'basePath' => dirname(__DIR__),

    'components' => [
        'user' => [
            'identityClass' => 'app\models\User',
        ],

        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'user',
                    'extraPatterns' => [
                        'POST register' => 'register',
                        'POST login' => 'login',
                        'GET allproducts' => 'allproducts',
                    ],
                ],
            ],
        ],
    ],
];
