<?php

namespace app\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\Response;

class UserController extends Controller
{
    public function actionRegister()
    {
        if (Yii::$app->request->isPost) {
            $username = Yii::$app->request->getBodyParam('username');
            $password = Yii::$app->request->getBodyParam('password');

            // Vérifier si le nom d'utilisateur ou le mot de passe est vide
            if (empty($username) || empty($password)) {
                return ['error' => 'Le nom d\'utilisateur et le mot de passe sont obligatoires.'];
            }

            // Logique de validation et d'enregistrement de l'utilisateur

            return ['message' =>  'Utilisateur enregistré avec succès'];
        } else {
            // Requête incorrecte
            Yii::$app->response->statusCode = 405; // Méthode non autorisée
            return ['error' => 'Méthode non autorisée. Seules les requêtes POST sont autorisées.'];
        }
    }

    public function actionLogin()
    {
        if (Yii::$app->request->isPost) {
            $username = Yii::$app->request->getBodyParam('username');
            $password = Yii::$app->request->getBodyParam('password');

            // Vérifier si le nom d'utilisateur ou le mot de passe est vide
            if (empty($username) || empty($password)) {
                return ['error' => 'Le nom d\'utilisateur et le mot de passe sont obligatoires.'];
            }

            // Logique d'authentification de l'utilisateur

            return ['message' => 'Authentification réussie', 'auth_token' => '...'];
        } else {
            // Requête incorrecte
            Yii::$app->response->statusCode = 405;
            return ['error' => 'Méthode non autorisée. Seules les requêtes POST sont autorisées.'];
        }
    }

    public function actionAllproducts()
    {
        if (Yii::$app->request->isGet) {
            $products = [
                ['id' => 1, 'name' => 'Electronic Device', 'reference' => 'ABC123', 'category' => 'Electronics', 'color' => 'Silver'],
                ['id' => 2, 'name' => 'Casual T-shirt', 'reference' => 'DEF456', 'category' => 'Clothing', 'color' => 'Blue'],
                ['id' => 3, 'name' => 'Running Shoes', 'reference' => 'GHI789', 'category' => 'Footwear', 'color' => 'Red'],
                ['id' => 4, 'name' => 'Coffee Maker', 'reference' => 'JKL012', 'category' => 'Electronics', 'color' => 'Black'],
                ['id' => 5, 'name' => 'Smart Watch', 'reference' => 'MNO345', 'category' => 'Electronics', 'color' => 'Black'],
                ['id' => 6, 'name' => 'Leather Wallet', 'reference' => 'PQR678', 'category' => 'Accessories', 'color' => 'Brown'],
                ['id' => 7, 'name' => 'Sunglasses', 'reference' => 'STU901', 'category' => 'Accessories', 'color' => 'Black'],
                ['id' => 8, 'name' => 'Electronic Device', 'reference' => 'ABC123', 'category' => 'Electronics', 'color' => 'Black'],
                ['id' => 9, 'name' => 'Casual T-shirt', 'reference' => 'DEF456', 'category' => 'Clothing', 'color' => 'Black'],
                ['id' => 10, 'name' => 'Running Shoes', 'reference' => 'GHI789', 'category' => 'Footwear', 'color' => 'White'],
                ['id' => 11, 'name' => 'Coffee Maker', 'reference' => 'JKL012', 'category' => 'Electronics', 'color' => 'Silver'],
                ['id' => 12, 'name' => 'Smart Watch', 'reference' => 'MNO345', 'category' => 'Electronics', 'color' => 'Silver'],
                ['id' => 13, 'name' => 'Leather Wallet', 'reference' => 'PQR678', 'category' => 'Accessories', 'color' => 'Black'],
                ['id' => 14, 'name' => 'Sunglasses', 'reference' => 'STU901', 'category' => 'Accessories', 'color' => 'Blue'],
                ['id' => 15, 'name' => 'Electronic Device', 'reference' => 'ABC123', 'category' => 'Electronics', 'color' => 'Red'],
                ['id' => 16, 'name' => 'Casual T-shirt', 'reference' => 'DEF456', 'category' => 'Clothing', 'color' => 'Gray'],
                ['id' => 17, 'name' => 'Running Shoes', 'reference' => 'GHI789', 'category' => 'Footwear', 'color' => 'Brown'],
                ['id' => 18, 'name' => 'Coffee Maker', 'reference' => 'JKL012', 'category' => 'Electronics', 'color' => 'White'],
                ['id' => 19, 'name' => 'Smart Watch', 'reference' => 'MNO345', 'category' => 'Electronics', 'color' => 'Gold'],
                ['id' => 20, 'name' => 'Leather Wallet', 'reference' => 'PQR678', 'category' => 'Accessories', 'color' => 'Tan'],
            ];

            Yii::$app->response->format = Response::FORMAT_JSON;

            return $products;
        } else {
            // Requête incorrecte
            Yii::$app->response->statusCode = 405; // Méthode non autorisée
            return ['error' => 'Méthode non autorisée. Seules les requêtes GET sont autorisées.'];
        }
    }
}
